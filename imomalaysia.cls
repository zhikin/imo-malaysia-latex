\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{imomalaysia}[2020/07/15 IMO Malaysia]
\RequirePackage{mathpazo}
\RequirePackage[T1]{fontenc}
\RequirePackage{graphicx}

\LoadClass[12pt,parskip,egregdoesnotlikesansseriftitles]{scrartcl}
\RequirePackage[margin=2cm]{geometry}
\pagestyle{empty}

\newcommand{\duration}[1]{\def\@duration{#1}}
\newcommand{\points}[1]{\def\@points{#1}}
\newcommand{\course}[1]{\def\@course{#1}}
\renewcommand{\author}[1]{\def\@author{#1}}

\RequirePackage{enumitem}
\setlist[enumerate]{leftmargin=5mm}

\RequirePackage{amsmath,amssymb}
\RequirePackage{textcomp}

\AtBeginDocument{
	\vspace*{-2.2cm}
	\begin{center}
		\raisebox{-5.4ex}{\includegraphics[width=4.6cm]{logo.png}}
		\hspace*{1.4cm}
		\begin{tabular}{l}
			\huge\bfseries \@course\\[0.8ex]
			\large\@title\\[0.8ex]
			\large\@date
		\end{tabular}
	
	\hrulefill
	\end{center}
}
\AtEndDocument{
	\par
	\vspace*{\stretch{1}}
	\hrulefill\\[2ex]
	\itshape\textcopyright\ IMO Malaysia Committee. \hfill Prepared by \@author.\\[-2ex]}


